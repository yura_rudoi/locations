package turvo.task.rudy.yury.location.net.comon;

import java.util.List;

import turvo.task.rudy.yury.location.model.LocationModel;

/**
 * Created by mac-204 on 9/14/17.
 */

public interface OnLocationLoaded {
    void onSuccess(List<LocationModel> locations);
    void onError();
}
