package turvo.task.rudy.yury.location;

import android.app.Application;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import turvo.task.rudy.yury.location.net.api.LocationApi;

/**
 * Created by mac-204 on 9/14/17.
 */

public class AppApplication extends Application {


    private static AppApplication instance;
    private Retrofit retrofit;
    private String baseUrl = "http://www.mocky.io/";
    private static LocationApi locationApi;

    @Override
    public void onCreate() {
        super.onCreate();

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                //         .addInterceptor(logging)
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        locationApi = retrofit.create(LocationApi.class);
    }


    public static AppApplication getContext() {
        return instance;
    }

    public static LocationApi getApi() {
        return locationApi;
    }

    public AppApplication() {
        instance = this;
    }
}
