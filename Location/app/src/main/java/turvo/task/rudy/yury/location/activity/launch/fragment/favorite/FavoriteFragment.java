package turvo.task.rudy.yury.location.activity.launch.fragment.favorite;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import turvo.task.rudy.yury.location.R;
import turvo.task.rudy.yury.location.activity.launch.LaunchActivity;
import turvo.task.rudy.yury.location.model.LocationModel;
import turvo.task.rudy.yury.location.net.LocationManager;
import turvo.task.rudy.yury.location.net.comon.OnLocationLoaded;

/**
 * Created by mac-204 on 9/12/17.
 */

public class FavoriteFragment extends Fragment implements OnLocationLoaded, View.OnClickListener {

    private RecyclerView recyclerView;
    private LinearLayout overlayView;

    public FavoriteFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.favorite_fragment, container, false);
        Button btnToLocation = (Button) view.findViewById(R.id.btnFirst);
        btnToLocation.setOnClickListener(this);
        overlayView = (LinearLayout) view.findViewById(R.id.overlayView);
        recyclerView = (RecyclerView) view.findViewById(R.id.rvFavoriteList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext());

        recyclerView.setLayoutManager(layoutManager);

        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getContext(), R.anim.layout_animation_fall_down);
        recyclerView.setLayoutAnimation(animation);
        LocationManager.getInstance().loadFavoriteLocations(this);
        return view;
    }

    @Override
    public void onSuccess(List<LocationModel> locations) {
        if (locations==null || locations.isEmpty()){
            overlayView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            overlayView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
        setAdapter(locations);
    }

    @Override
    public void onError() {

    }

    private void setAdapter(List<LocationModel> locations) {
        FavoriteRecyclerViewAdapter adapter = new FavoriteRecyclerViewAdapter(locations);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        ((LaunchActivity) getActivity()).toLocationTab();
    }


    private class FavoriteRecyclerViewAdapter extends RecyclerView.Adapter<LocationViewHolder> {

        private final List<LocationModel> items;


        private FavoriteRecyclerViewAdapter(List<LocationModel> items) {
            this.items = items;
        }

        @Override
        public LocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.location_list_item_card, parent, false);
            return new LocationViewHolder(v);
        }

        @Override
        public void onBindViewHolder(LocationViewHolder holder, int position) {
            LocationModel item = items.get(position);
            holder.locationName.setText(item.getName());
            Picasso.with(getContext()).load(item.getBackgroundImageUrl()).into(holder.locationImage);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }
    }


    private class LocationViewHolder extends RecyclerView.ViewHolder {

        private final TextView locationName;
        private final ImageView locationImage;

        public LocationViewHolder(View itemView) {
            super(itemView);
            locationName = (TextView) itemView.findViewById(R.id.tvLocationName);
            locationImage = (ImageView) itemView.findViewById(R.id.ivBackroundImage);
        }


    }

}
