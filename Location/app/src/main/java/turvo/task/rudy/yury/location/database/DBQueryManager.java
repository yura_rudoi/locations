package turvo.task.rudy.yury.location.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import turvo.task.rudy.yury.location.model.LocationModel;
import turvo.task.rudy.yury.location.model.POI;

/**
 * Created by mac-204 on 9/15/17.
 */

public class DBQueryManager {
    private SQLiteDatabase database;

     DBQueryManager(SQLiteDatabase database) {
        this.database = database;
    }

    public List<LocationModel> getLocations() {
        List<LocationModel> locations = new ArrayList<>();

        Cursor cursor = database.query(DBHelper.LOCATION_TABLE, null, null, null, null, null, null);

        if (cursor.moveToFirst()) {
            do {
                String id = cursor.getString(cursor.getColumnIndex(DBHelper.LOCATION_ID_COLUMN));
                String name = cursor.getString(cursor.getColumnIndex(DBHelper.LOCATION_NAME_COLUMN));
                String icon = cursor.getString(cursor.getColumnIndex(DBHelper.LOCATION_ICON_COLUMN));
                String backgroundImage = cursor.getString(cursor.getColumnIndex(DBHelper.LOCATION_BACKGROUND_IMAGE_COLUMN));
                double latitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex(DBHelper.LOCATION_LATITUDE_COLUMN)));
                double longitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex(DBHelper.LOCATION_LONGITUDE_COLUMN)));
                boolean isFavorite = Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(DBHelper.LOCATION_IS_FAVORITE_COLUMN)));
                long lastModifier = cursor.getLong(cursor.getColumnIndex(DBHelper.LOCATION_LAST_MODIFIER_COLUMN));
                List<POI> pois = getPOIs(id);

                LocationModel locationModel = new LocationModel(id, name, icon, backgroundImage, latitude, longitude, isFavorite, lastModifier, pois);

                locations.add(locationModel);
            } while (cursor.moveToNext());

        }
        cursor.close();
        return locations;
    }

    public List<LocationModel> getFavoriteLocations() {
        List<LocationModel> locations = new ArrayList<>();

        Cursor cursor = database.query(DBHelper.LOCATION_TABLE, null, DBHelper.LOCATION_IS_FAVORITE_COLUMN + " = ?", new String[]{"true"},
                null, null, DBHelper.LOCATION_LAST_MODIFIER_COLUMN + " desc");

        if (cursor.moveToFirst()) {
            do {
                String id = cursor.getString(cursor.getColumnIndex(DBHelper.LOCATION_ID_COLUMN));
                String name = cursor.getString(cursor.getColumnIndex(DBHelper.LOCATION_NAME_COLUMN));
                String icon = cursor.getString(cursor.getColumnIndex(DBHelper.LOCATION_ICON_COLUMN));
                String backgroundImage = cursor.getString(cursor.getColumnIndex(DBHelper.LOCATION_BACKGROUND_IMAGE_COLUMN));
                double latitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex(DBHelper.LOCATION_LATITUDE_COLUMN)));
                double longitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex(DBHelper.LOCATION_LONGITUDE_COLUMN)));
                boolean isFavorite = Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(DBHelper.LOCATION_IS_FAVORITE_COLUMN)));
                long lastModifier = cursor.getLong(cursor.getColumnIndex(DBHelper.LOCATION_LAST_MODIFIER_COLUMN));
                List<POI> pois = getPOIs(id);

                LocationModel locationModel = new LocationModel(id, name, icon, backgroundImage, latitude, longitude, isFavorite, lastModifier, pois);

                locations.add(locationModel);
            } while (cursor.moveToNext());

        }
        cursor.close();
        return locations;
    }




    private List<POI> getPOIs(String locationId) {

        List<POI> pois = new ArrayList<>();

        Cursor cursor = database.query(DBHelper.POI_TABLE, null, DBHelper.POI_LOCATION_ID_COLUMN + " = ?", new String[]{locationId}, null, null, null);


        if (cursor.moveToFirst()) {
            do {
                String name = cursor.getString(cursor.getColumnIndex(DBHelper.POI_NAME_COLUMN));
                String backgroundImage = cursor.getString(cursor.getColumnIndex(DBHelper.POI_BACKGROUND_IMAGE_COLUMN));
                double latitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex(DBHelper.POI_LATITUDE_COLUMN)));
                double longitude = Double.parseDouble(cursor.getString(cursor.getColumnIndex(DBHelper.POI_LONGITUDE_COLUMN)));

                POI poi = new POI(name, backgroundImage, latitude, longitude);
                pois.add(poi);

            } while (cursor.moveToNext());

        }
        cursor.close();
        return pois;

    }
}
