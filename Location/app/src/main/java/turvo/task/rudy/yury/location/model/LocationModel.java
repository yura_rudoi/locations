package turvo.task.rudy.yury.location.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mac-204 on 9/12/17.
 */

public class LocationModel implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("icon")
    @Expose
    private String iconUrl;
    @SerializedName("backgroundImage")
    @Expose
    private String backgroundImageUrl;
    @SerializedName("latitude")
    @Expose
    private double latitude;
    @SerializedName("longitude")
    @Expose
    private double longitude;
    @SerializedName("pois")
    @Expose
    private List<POI> poiList;

    private boolean isFavorite;
    private long lastModifier;


    public LocationModel() {
    }

    public LocationModel(String id, String name, String icon, String backgroundImage, double latitude, double longitude, boolean isFavorite, long lastModifier, List<POI> pois) {

        this.id = id;
        this.name = name;
        this.iconUrl = icon;
        this.backgroundImageUrl = backgroundImage;
        this.latitude = latitude;
        this.longitude = longitude;
        this.isFavorite = isFavorite;
        this.lastModifier = lastModifier;
        this.poiList = pois;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getBackgroundImageUrl() {
        return backgroundImageUrl;
    }

    public void setBackgroundImageUrl(String backgroundImageUrl) {
        this.backgroundImageUrl = backgroundImageUrl;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longtitudeLoc) {
        this.longitude = longtitudeLoc;
    }

    public List<POI> getPoiList() {
        return poiList;
    }

    public void setPoiList(List<POI> poiList) {
        this.poiList = poiList;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getLastModifier() {
        return lastModifier;
    }

    public void setLastModifier(long lastModifier) {
        this.lastModifier = lastModifier;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.iconUrl);
        dest.writeString(this.backgroundImageUrl);
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
        dest.writeTypedList(this.poiList);
        dest.writeByte(this.isFavorite ? (byte) 1 : (byte) 0);
        dest.writeString(this.id);
        dest.writeLong(this.lastModifier);
    }

    protected LocationModel(Parcel in) {
        this.name = in.readString();
        this.iconUrl = in.readString();
        this.backgroundImageUrl = in.readString();
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
        this.poiList = in.createTypedArrayList(POI.CREATOR);
        this.isFavorite = in.readByte() != 0;
        this.id = in.readString();
        this.lastModifier = in.readLong();
    }

    public static final Creator<LocationModel> CREATOR = new Creator<LocationModel>() {
        @Override
        public LocationModel createFromParcel(Parcel source) {
            return new LocationModel(source);
        }

        @Override
        public LocationModel[] newArray(int size) {
            return new LocationModel[size];
        }
    };
}
