package turvo.task.rudy.yury.location.util;

import android.util.Log;

/**
 * Created by mac-204 on 9/14/17.
 */

public class LogUtil {

    private static final String TAG = "Location";

    public static void log(String message) {
        Log.d(TAG, message);
    }

    public static void err(String message, Throwable throwable) {
        Log.e(TAG, message, throwable);
    }
}
