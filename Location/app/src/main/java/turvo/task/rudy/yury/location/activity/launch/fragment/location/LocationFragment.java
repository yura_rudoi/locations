package turvo.task.rudy.yury.location.activity.launch.fragment.location;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import turvo.task.rudy.yury.location.R;
import turvo.task.rudy.yury.location.activity.detail.DetailLocationActivity;
import turvo.task.rudy.yury.location.model.LocationModel;
import turvo.task.rudy.yury.location.net.LocationManager;
import turvo.task.rudy.yury.location.net.comon.OnLocationLoaded;

/**
 * Created by mac-204 on 9/12/17.
 */

public class LocationFragment extends Fragment implements OnLocationLoaded {

    private RecyclerView recyclerView;
    private static final int REQUEST_CODE_FAVORITE = 1;


    public LocationFragment() {
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.location_fragment, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.rvLocationList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        //StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getContext(), R.anim.layout_animation_fall_down);
        recyclerView.setLayoutAnimation(animation);

        LocationManager.getInstance().loadLocations(this);
        return view;
    }

    @Override
    public void onSuccess(List<LocationModel> locations) {
        setAdapter(locations);
    }

    @Override
    public void onError() {

    }

    private void setAdapter(List<LocationModel> locations){
        LocationRecyclerViewAdapter adapter = new LocationRecyclerViewAdapter(locations);
        recyclerView.setAdapter(adapter);
    }


    private class LocationRecyclerViewAdapter extends RecyclerView.Adapter<LocationViewHolder> {

        private final List<LocationModel> items;


        public LocationRecyclerViewAdapter(List<LocationModel> items) {
            this.items = items;
        }

        @Override
        public LocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.location_list_item_card_main, parent, false);
            return new LocationViewHolder(v);
        }

        @Override
        public void onBindViewHolder(LocationViewHolder holder, int position) {
            LocationModel item = items.get(position);
            holder.locationModel = item;
            holder.locationName.setText(item.getName());
            Picasso.with(getContext()).load(item.getBackgroundImageUrl()).into(holder.locationImage);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }
    }


    private class LocationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final TextView locationName;
        private final ImageView locationImage;
        private  LocationModel locationModel;

        public LocationViewHolder(View itemView) {
            super(itemView);
            locationName = (TextView) itemView.findViewById(R.id.tvLocationName);
            locationImage = (ImageView) itemView.findViewById(R.id.ivBackroundImage);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            startActivityForResult(DetailLocationActivity.getIntentForStart(getContext(),locationModel), REQUEST_CODE_FAVORITE);
        }
    }
}
