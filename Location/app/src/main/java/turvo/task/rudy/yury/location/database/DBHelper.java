package turvo.task.rudy.yury.location.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import turvo.task.rudy.yury.location.util.LogUtil;

/**
 * Created by mac-204 on 9/15/17.
 */

public class DBHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "location_database";

    public static final String LOCATION_TABLE = "location_table";
    public static final String POI_TABLE = "poi_table";


    private DBQueryManager queryManager;
    private DBUpdateManager updateManager;
    private DBInsertManager insertManager;

    // LOCATION TABLE
    public static final String LOCATION_ID_COLUMN = "location_id";
    public static final String LOCATION_NAME_COLUMN = "location_name";
    public static final String LOCATION_ICON_COLUMN = "location_icon";
    public static final String LOCATION_BACKGROUND_IMAGE_COLUMN = "location_background_image";
    public static final String LOCATION_LATITUDE_COLUMN = "location_latitude";
    public static final String LOCATION_LONGITUDE_COLUMN = "location_longitude";
    public static final String LOCATION_IS_FAVORITE_COLUMN = "location_is_favorite";
    public static final String LOCATION_LAST_MODIFIER_COLUMN = "location_last_modifier";

    // POI TABLE
    public static final String POI_NAME_COLUMN = "poi_name";
    public static final String POI_BACKGROUND_IMAGE_COLUMN = "poi_background_image";
    public static final String POI_LATITUDE_COLUMN = "poi_latitude";
    public static final String POI_LONGITUDE_COLUMN = "poi_longitude";
    public static final String POI_LOCATION_ID_COLUMN = "poi_location_id";


    private static final String LOCATION_TABLE_CREATE_SCRIPT = "CREATE TABLE "
            + LOCATION_TABLE + " (" + LOCATION_ID_COLUMN
            + " TEXT PRIMARY KEY , " + LOCATION_NAME_COLUMN + " TEXT, " + LOCATION_ICON_COLUMN + " TEXT, "
            + LOCATION_BACKGROUND_IMAGE_COLUMN + " TEXT, " + LOCATION_LATITUDE_COLUMN + " TEXT, " + LOCATION_LONGITUDE_COLUMN + " TEXT, "
            + LOCATION_IS_FAVORITE_COLUMN + " TEXT, " + LOCATION_LAST_MODIFIER_COLUMN + " LONG);";

    private static final String POI_TABLE_CREATE_SCRIPT = "CREATE TABLE "
            + POI_TABLE + " (" + BaseColumns._ID
            + " INTEGER PRIMARY KEY AUTOINCREMENT, " + POI_NAME_COLUMN + " TEXT, " + POI_BACKGROUND_IMAGE_COLUMN + " TEXT, "
            + POI_LATITUDE_COLUMN + " TEXT, " + POI_LONGITUDE_COLUMN + " TEXT, " + POI_LOCATION_ID_COLUMN + " TEXT NOT NULL);";


    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        LogUtil.log("DBHELPER");
        queryManager = new DBQueryManager(getReadableDatabase());
        updateManager = new DBUpdateManager(getWritableDatabase());
       insertManager = new DBInsertManager(getWritableDatabase());
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
          sqLiteDatabase.execSQL(LOCATION_TABLE_CREATE_SCRIPT);
         sqLiteDatabase.execSQL(POI_TABLE_CREATE_SCRIPT);
        LogUtil.log("database OnCreate");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + LOCATION_TABLE);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + POI_TABLE);
    }

    public DBQueryManager query() {
        return queryManager;
    }

    public DBUpdateManager update() {
        return updateManager;
    }

    public DBInsertManager insert() {
        return insertManager;
    }
}
