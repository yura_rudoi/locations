package turvo.task.rudy.yury.location.database;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import turvo.task.rudy.yury.location.model.LocationModel;
import turvo.task.rudy.yury.location.model.POI;

/**
 * Created by mac-204 on 9/15/17.
 */

public class DBInsertManager {

    private SQLiteDatabase database;

     DBInsertManager(SQLiteDatabase database) {
        this.database = database;
    }


    public void saveLocations(List<LocationModel> locations) {

        for (int i = 0; i < locations.size(); i++) {
            ContentValues newValues = new ContentValues();
            LocationModel location = locations.get(i);

            newValues.put(DBHelper.LOCATION_ID_COLUMN, location.getId());
            newValues.put(DBHelper.LOCATION_NAME_COLUMN, location.getName());
            newValues.put(DBHelper.LOCATION_ICON_COLUMN, location.getIconUrl());
            newValues.put(DBHelper.LOCATION_BACKGROUND_IMAGE_COLUMN, location.getBackgroundImageUrl());
            newValues.put(DBHelper.LOCATION_LATITUDE_COLUMN, location.getLatitude());
            newValues.put(DBHelper.LOCATION_LONGITUDE_COLUMN, location.getLongitude());
            newValues.put(DBHelper.LOCATION_IS_FAVORITE_COLUMN, "false");
            newValues.put(DBHelper.LOCATION_LAST_MODIFIER_COLUMN, System.currentTimeMillis());

            savePOIs(location.getPoiList(), location.getId());
            database.insert(DBHelper.LOCATION_TABLE, null, newValues);

        }
    }


    private void savePOIs(List<POI> pois, String locationId) {

        for (int i = 0; i < pois.size(); i++) {
            ContentValues newValues = new ContentValues();
            POI poi = pois.get(i);

            newValues.put(DBHelper.POI_LOCATION_ID_COLUMN, locationId);
            newValues.put(DBHelper.POI_NAME_COLUMN, poi.getName());
            newValues.put(DBHelper.POI_BACKGROUND_IMAGE_COLUMN, poi.getBackgroundImageUrl());
            newValues.put(DBHelper.POI_LATITUDE_COLUMN, poi.getLatitude());
            newValues.put(DBHelper.POI_LONGITUDE_COLUMN, poi.getLongitude());

            database.insert(DBHelper.POI_TABLE, null, newValues);

        }

    }

}
