package turvo.task.rudy.yury.location.activity.detail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.util.List;

import turvo.task.rudy.yury.location.R;
import turvo.task.rudy.yury.location.database.DBProvider;
import turvo.task.rudy.yury.location.model.LocationModel;
import turvo.task.rudy.yury.location.model.POI;

import static turvo.task.rudy.yury.location.AppApplication.getContext;

/**
 * Created by mac-204 on 9/13/17.
 */

public class DetailLocationActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener {

    private GoogleMap mMap;
    private LocationModel location;
    private final static String DETAIL_LOCATION = "detail_location";

    public static Intent getIntentForStart(Context context, LocationModel location) {
        Intent intent = new Intent(context, DetailLocationActivity.class);
        intent.putExtra(DETAIL_LOCATION, location);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location_detail_activity);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        location = getIntent().getExtras().getParcelable(DETAIL_LOCATION);

        TextView detailName = (TextView) findViewById(R.id.tvDetailName);
        detailName.setText(location.getName());
        ImageView backGroundImage = (ImageView) findViewById(R.id.ivBackroundImage);
        ImageView backBtn = (ImageView) findViewById(R.id.ivBack);
        ImageView favoriteBtn = (ImageView) findViewById(R.id.ivFavorite);
        favoriteBtn.setImageResource(location.isFavorite() ? R.drawable.favorite_btn_selected : R.drawable.favorite_btn_selector);
        backBtn.setOnClickListener(this);
        favoriteBtn.setOnClickListener(this);

        Picasso.with(this).load(location.getBackgroundImageUrl()).into(backGroundImage);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rvLocationDetail);
      //  LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getContext(), R.anim.layout_animation_fall_down);
        recyclerView.setLayoutAnimation(animation);

        LocationRecyclerViewAdapter adapter = new LocationRecyclerViewAdapter(location.getPoiList());
        recyclerView.setAdapter(adapter);


    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng marker = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.addMarker(new MarkerOptions().position(marker).title(location.getName()));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(marker));
        CameraPosition cameraPosition = new CameraPosition.Builder().target(marker).zoom(+14).build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.ivBack) {
            onBackPressed();
        } else if (view.getId() == R.id.ivFavorite) {
            DBProvider.getDBHelper().update().updateIsFavorite(location.getId(), location.isFavorite());
            setResult(RESULT_OK);
            finish();
        }
    }


    private class LocationRecyclerViewAdapter extends RecyclerView.Adapter<LocationViewHolder> {

        private final List<POI> items;


        public LocationRecyclerViewAdapter(List<POI> items) {
            this.items = items;
        }

        @Override
        public LocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.location_list_item_card, parent, false);
            return new LocationViewHolder(v);
        }

        @Override
        public void onBindViewHolder(LocationViewHolder holder, int position) {
            POI item = items.get(position);
            holder.locationName.setText(item.getName());
            Picasso.with(DetailLocationActivity.this).load(item.getBackgroundImageUrl()).into(holder.locationImage);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }
    }


    private class LocationViewHolder extends RecyclerView.ViewHolder {

        private final TextView locationName;
        private final ImageView locationImage;

        public LocationViewHolder(View itemView) {
            super(itemView);
            locationName = (TextView) itemView.findViewById(R.id.tvLocationName);
            locationImage = (ImageView) itemView.findViewById(R.id.ivBackroundImage);
        }
    }
}