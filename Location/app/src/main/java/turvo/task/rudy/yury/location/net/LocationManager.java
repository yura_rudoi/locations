package turvo.task.rudy.yury.location.net;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import turvo.task.rudy.yury.location.AppApplication;
import turvo.task.rudy.yury.location.database.DBProvider;
import turvo.task.rudy.yury.location.model.LocationModel;
import turvo.task.rudy.yury.location.net.comon.OnLocationLoaded;
import turvo.task.rudy.yury.location.util.LogUtil;

import static turvo.task.rudy.yury.location.database.DBProvider.getDBHelper;

/**
 * Created by mac-204 on 9/14/17.
 */

public class LocationManager {

    private static LocationManager instance;

    private List<LocationModel> locations;
    private List<OnLocationLoaded> loads;

    public static LocationManager getInstance() {
        if (instance == null) {
            instance = new LocationManager();
        }
        return instance;
    }

    public LocationManager() {
        locations = new ArrayList<>();
        loads = new ArrayList<>();
    }

    public void loadLocations(OnLocationLoaded locationLoaded) {

        locations.clear();
        locations.addAll(DBProvider.getDBHelper().query().getLocations());
        if (locations != null && !locations.isEmpty()) {
            locationLoaded.onSuccess(locations);
            return;
        }

        if (loads.isEmpty()) {
            loads.add(locationLoaded);
            getLocationsFromNetwork();
        } else {
            loads.add(locationLoaded);
        }

    }

    public void loadFavoriteLocations(OnLocationLoaded locationLoaded) {

        List<LocationModel> favoriteLocations = DBProvider.getDBHelper().query().getFavoriteLocations();
        if (favoriteLocations != null) {
            locationLoaded.onSuccess(favoriteLocations);
        } else locationLoaded.onError();


    }

    public void setLocations(List<LocationModel> locations) {
        this.locations = locations;
    }


    private void getLocationsFromNetwork() {

        AppApplication.getApi().getLocations().enqueue(new Callback<List<LocationModel>>() {
            @Override
            public void onResponse(Call<List<LocationModel>> call, Response<List<LocationModel>> response) {
                locations.clear();
                locations.addAll(response.body());
                getDBHelper().insert().saveLocations(locations);

                for (OnLocationLoaded loaded : loads) {
                    loaded.onSuccess(locations);
                }
            }

            @Override
            public void onFailure(Call<List<LocationModel>> call, Throwable t) {
                LogUtil.err(t.getMessage(), t);
                for (OnLocationLoaded loaded : loads) {
                    loaded.onError();
                }
                loads.clear();
            }
        });

    }

}

