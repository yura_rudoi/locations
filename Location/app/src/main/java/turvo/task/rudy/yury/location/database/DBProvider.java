package turvo.task.rudy.yury.location.database;

import android.content.Context;

import turvo.task.rudy.yury.location.AppApplication;

/**
 * Created by mac-204 on 9/15/17.
 */

public class DBProvider {

    private static DBHelper dbHelper;

    private DBProvider(Context context) {
    }

    public static DBHelper getDBHelper() {
        if (dbHelper == null) {
            dbHelper = new DBHelper(AppApplication.getContext());
        }
        return dbHelper;
    }
}

