package turvo.task.rudy.yury.location.net.api;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import turvo.task.rudy.yury.location.model.LocationModel;

/**
 * Created by mac-204 on 9/14/17.
 */

public interface LocationApi {

    @GET("v2/59bbacf10f0000e001ff86d0")
    Call<List<LocationModel>> getLocations();
}
