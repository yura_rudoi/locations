package turvo.task.rudy.yury.location.activity.launch;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;

import turvo.task.rudy.yury.location.R;
import turvo.task.rudy.yury.location.activity.launch.fragment.favorite.FavoriteFragment;
import turvo.task.rudy.yury.location.activity.launch.fragment.location.LocationFragment;
import turvo.task.rudy.yury.location.activity.launch.fragment.map.MapFragment;
import turvo.task.rudy.yury.location.framework.BaseTabsActivity;

public class LaunchActivity extends BaseTabsActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.launch);

        init();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowTitleEnabled(false);
        }
    }

    @Override
    protected void setupViewPager(final ViewPagerAdapter adapter) {
        adapter.addFragment(new LocationFragment(), getString(R.string.launch_tab_location_name));
        adapter.addFragment(new MapFragment(), getString(R.string.launch_tab_map_name));
        adapter.addFragment(new FavoriteFragment(), getString(R.string.launch_tab_favorite_name));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            viewPager.setCurrentItem(2, false);
        }

    }

    public void toLocationTab(){
        viewPager.setCurrentItem(0, false);
    }
}
