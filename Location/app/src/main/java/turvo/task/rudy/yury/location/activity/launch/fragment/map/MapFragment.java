package turvo.task.rudy.yury.location.activity.launch.fragment.map;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import turvo.task.rudy.yury.location.R;
import turvo.task.rudy.yury.location.model.LocationModel;
import turvo.task.rudy.yury.location.net.LocationManager;
import turvo.task.rudy.yury.location.net.comon.OnLocationLoaded;

/**
 * Created by mac-204 on 9/12/17.
 */

public class MapFragment extends Fragment implements OnLocationLoaded {

    private MapView mapView;
    private GoogleMap googleMap;
    private List<LocationModel> locations;


    public MapFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.map_fragment, container, false);

        mapView = (MapView) view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;
                LocationManager.getInstance().loadLocations(MapFragment.this);
                LatLng minsk = new LatLng(53.9000000, 27.5666700);
                CameraPosition cameraPosition = new CameraPosition.Builder().target(minsk).zoom(12).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });

        return view;
    }
    @Override
    public void onSuccess(List<LocationModel> locations) {
        this.locations = locations;
        addMarker();
    }

    @Override
    public void onError() {

    }

    private void addMarker() {
        for (LocationModel location : locations) {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            googleMap.addMarker(new MarkerOptions().position(latLng).title(location.getName()));
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


}
