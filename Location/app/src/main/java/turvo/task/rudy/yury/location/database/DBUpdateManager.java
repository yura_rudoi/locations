package turvo.task.rudy.yury.location.database;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by mac-204 on 9/15/17.
 */

public class DBUpdateManager {

    SQLiteDatabase database;

    public DBUpdateManager(SQLiteDatabase database) {
        this.database = database;
    }

    public void updateIsFavorite(String locationId , boolean isFavorite ){
        ContentValues cv = new ContentValues();
        cv.put(DBHelper.LOCATION_IS_FAVORITE_COLUMN, String.valueOf(!isFavorite));
        cv.put(DBHelper.LOCATION_LAST_MODIFIER_COLUMN, System.currentTimeMillis());
        database.update(DBHelper.LOCATION_TABLE, cv, DBHelper.LOCATION_ID_COLUMN + " = " + locationId, null);

    }
}
